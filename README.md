[![Testspace](http://www.s2tec.com/public/img/testspace_logo.png)](http://www.testspace.com)

***

## Ruby/Minitest sample for demonstrating Testspace 

This is the sample application for the [*Ruby on Rails Tutorial: Learn Web Development with Rails*](http://www.railstutorial.org/) by [Michael Hartl](http://www.michaelhartl.com/). It is being used to demonstrate Testspace  publishing test content. 
We made a few minor modifications for reporting purposes. 

***
Publishing **Test Content** using www.testspace.com.

[![Space Health](http://munderseth.stridespace.com/projects/283/spaces/1741/badge)](http://munderseth.stridespace.com/projects/283/spaces/1741 "Test Cases")

***
In order to run this sample you will need a host workstation that supports the [Minitest test framework](http://docs.seattlerb.org/minitest/). 


Running Static Analysis: 



```
#!sh

bundle install
bundle exec rubocop --format emacs --out tmp/rubocop.txt
bundle exec brakeman -o tmp/brakeman.json
bundle exec brakeman_translate_checkstyle_format translate --file="tmp/brakeman.json" > tmp/brakeman_checkstyle.xml
bundle exec scss-lint --no-color --format=Stats --format=Default --out=tmp/scss-lint.txt  app/assets/stylesheets/


```

Running Tests with Code Coverage: 



```
#!sh

export CI_REPORTS=$PWD/test/reports
bundle exec rake minitest test

```
 

Publishing Results using **Testspace**: 



```
#!sh

curl -s https://testspace-client.s3.amazonaws.com/testspace-linux.tgz | sudo tar -zxvf- -C /usr/local/bin
testspace @.testspace $TESTSPACE_TOKEN/$BRANCH_NAME
 

```

Checkout the [Space](http://munderseth.stridespace.com/spaces/883). Note that the `.testspace` file contains the set of files to publish. 



***

To replicate this sample: 

  - Account at www.testspace.com.

  - CI Environment Variable called **TESTSPACE_TOKEN** required:
    -  `TESTSPACE_TOKEN` = `credentials@my-org-name.testspace.com/my-project`
    - `credentials` set to `username:password` or your [access token](http://help.testspace.com/using-your-organization:user-settings).
    - `my-org-name.testspace.com/my-project` based on your *subdomain* and *project* names. Refer [here](http://help.testspace.com/reference:runner-reference#login-credentials) for more details.